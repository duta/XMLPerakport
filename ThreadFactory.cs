﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace XMLPerakport
{
    internal class ThreadFactory
    {
        public delegate void TaskError(Task task, Exception error);

        public static readonly ThreadFactory Instance = new ThreadFactory();

        private ThreadFactory() { }

        public event TaskError Error;

        public void InvokeError(Task task, Exception error)
        {
            TaskError handler = Error;
            if (handler != null) handler(task, error);
        }

        public void Start(Action action)
        {
            var task = new Task(action);
            Start(task);
        }

        public void Start(Action action, TaskCreationOptions options)
        {
            var task = new Task(action, options);
            Start(task);
        }

        private void Start(Task task)
        {
            task.ContinueWith(t => InvokeError(t, t.Exception.InnerException),
                                TaskContinuationOptions.OnlyOnFaulted |
                                TaskContinuationOptions.ExecuteSynchronously);
            task.Start();
        }
    }
}
