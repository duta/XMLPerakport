﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.ServiceProcess;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace XMLPerakport
{
    class Program
    {
        internal static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            // Sets the culture to US
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            // Sets the UI culture to US
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            ThreadFactory.Instance.Error += ThreadFactory_Error;

            //wait for OS system established
            Thread.Sleep(10000);

            if (bool.Parse(ConfigurationManager.AppSettings["RunAsService"].ToString())) {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
				{ 
					new XMLGeneratorService() 
				};
                ServiceBase.Run(ServicesToRun);                  
            } else {
                XMLGeneratorService svc = new XMLGeneratorService();
                svc.GoStart(args);

                ManualResetEvent mre = new ManualResetEvent(false);
                Console.CancelKeyPress += (sender, e) =>
                {
                    mre.Set();
                };
                mre.WaitOne();
                return;    
            }
        }

        static void ThreadFactory_Error(Task task, Exception error)
        {
            logger.Error("ThreadFactory Error", error);
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            logger.Info("Closing application : " + DateTime.Now.ToString("G"));
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            logger.Error("Domain catch error : " + ex.Message, ex);
        }
    }

    public class XMLGeneratorService : ServiceBase{

        XMLPerakportEngine _xpe;

        public XMLGeneratorService() 
		{
			this.ServiceName = "XMLGenerator Perakport";
			this.CanStop = true;
			this.CanPauseAndContinue = false;
			this.AutoLog = true;
			
		}

        public void GoStart(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            Thread.Sleep(10000); // Wait for Other service started

            _xpe = new XMLPerakportEngine();
            _xpe.Start();
           
            this.EventLog.WriteEntry("XMLGeneratorService Start.");
        }

        protected override void OnStop()
        {
            _xpe.Stop();
            this.EventLog.WriteEntry("XMLGeneratorService Stop.");
        }
    }
}
