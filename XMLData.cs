﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Data;
using Dapper;

namespace XMLPerakport
{
    public class XMLData
    {
        private DBProvider.Oracle _ora;

        public XMLData(string connString) {
            _ora = new DBProvider.Oracle(connString);
        }

        public XMLData Instance(string connString)
        {
            return new XMLData(connString);
        }

        public IEnumerable<Entity.XMLPenetapanLabuh> GetPenetapanLabuh() {
            StringBuilder sb = new StringBuilder();
            sb.Append("  select tppkb1_nama_kapal nama_kapal, ");
            sb.Append("         tppkb1_bendera bendera, ");
            sb.Append("         pnt_mulai mulai, ");
            sb.Append("         pnt_selesai selesai, ");
            sb.Append("         tppkb1_jenis_kapal jenis_kapal, ");
            sb.Append("         mplg_nama agen  ");
            sb.Append("  from ");
            sb.Append("         upkt_pmh_ppkb1 a,safm_pelanggan b,view_monitoring c ");
            sb.Append("  where a.mplg_kode=b.mplg_kode ");
            sb.Append("  and pmh_jns_jasa='LABUH' ");
            sb.Append("  and pmh_tppkb1=a.tppkb1_nomor ");
            sb.Append("  and to_char(pnt_mulai,'ddmmrrrr')=to_char(sysdate,'ddmmrrrr') ");
            _ora.OpenConnectionAndCreateCommand();
            var result = _ora.Conn.Query<Entity.XMLPenetapanLabuh>(sb.ToString());
            _ora.CloseConnection();
            return result;
        }

        public IEnumerable<Entity.XMLPenetapanTambat> GetPenetapanTambat() {
            StringBuilder sb = new StringBuilder();
            sb.Append("  select tppkb1_nama_kapal nama_kapal, ");
            sb.Append("         tppkb1_bendera bendera, ");
            sb.Append("         pnt_mulai mulai, ");
            sb.Append("         pnt_selesai selesai, ");
            sb.Append("         tppkb1_jenis_kapal jenis_kapal, ");
            sb.Append("         mplg_nama agen, ");
            sb.Append("         mdmg_nama dermaga  ");
            sb.Append("  from ");
            sb.Append("         upkt_pmh_ppkb1 a,safm_pelanggan b,view_monitoring c,upkm_dermaga d ");
            sb.Append("  where a.mplg_kode=b.mplg_kode ");
            sb.Append("  and pmh_jns_jasa='TAMBAT' ");
            sb.Append("  and pmh_tppkb1=a.tppkb1_nomor ");
            sb.Append("  and pnt_lok_akhir=mdmg_kode ");
            sb.Append("  and to_char(pnt_mulai,'ddmmrrrr')=to_char(sysdate,'ddmmrrrr') ");
            _ora.OpenConnectionAndCreateCommand();
            var result = _ora.Conn.Query<Entity.XMLPenetapanTambat>(sb.ToString());
            _ora.CloseConnection();
            return result;
        }
    }
}
