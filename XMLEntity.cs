﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLPerakport.Entity
{
    public class XMLPenetapan
    {
        public string nama_kapal{get;set;}
        public string bendera { get; set; }
        public DateTime mulai { get; set; }
        public DateTime selesai { get; set; }
        public string jenis_kapal { get; set; }
        public string agen { get; set; }
    }

    public class XMLPenetapanLabuh : XMLPenetapan
    { }

    public class XMLPenetapanTambat : XMLPenetapan
    {
        public string dermaga { get; set; }
    }
}
