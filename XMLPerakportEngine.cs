﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;
using System.IO;
using System.Threading;
using log4net;
using WinSCP;

namespace XMLPerakport
{
    public class XMLPerakportEngine
    {
        internal readonly ILog logger = LogManager.GetLogger(typeof(XMLPerakportEngine));
        private string _connString;
        private bool _start;
        
        public XMLPerakportEngine() {
            _connString = ConfigurationManager.ConnectionStrings["ora_siuk"].ConnectionString;            
        }

        public void Start()
        {
            _start = true;
            ThreadFactory.Instance.Start(new Action(StartGenerate));
        }

        public void Stop()
        {
            _start = false;
        }

        public void StartGenerate() {
            while (_start)
            {
                WriteToFile(GenerateXMLPenetapanLabuh(), "XMLGen_Renc_Labuh");
                UploadFile("XMLGen_Renc_Labuh.xml");
                WriteToFile(GenerateXMLPenetapanTambat(), "XMLGen_Renc_Tambat");
                UploadFile("XMLGen_Renc_Tambat.xml");

                Thread.Sleep(int.Parse( ConfigurationManager.AppSettings["intervalGetDataSiuk"].ToString()) * 60000);
            }
        }

        public string GenerateXMLPenetapanLabuh() {
            logger.Info("Start GenerateXMLPenetapanLabuh");
            StringBuilder sb = new StringBuilder();
            XMLData xd = new XMLData(_connString);
            IEnumerable<Entity.XMLPenetapanLabuh> listLabuh = xd.GetPenetapanLabuh();

            sb.Append("<?xml version=\"1.0\" ?>");
            sb.Append("<XMLGen_Renc_Labuh>");
            foreach (var item in listLabuh)
            {
                sb.Append("<XMLGen_Renc_Labuh_row>");
                sb.Append(" <nama_kapal>"+ item.nama_kapal +"</nama_kapal> ");
                sb.Append(" <bendera>"+ item.bendera +"</bendera> ");
                sb.Append(" <upkt_pnt_labuh_mulai_labuh>"+ item.mulai.ToString("dd/MM/yyyy HH:mm") +"</upkt_pnt_labuh_mulai_labuh> ");
                sb.Append(" <upkt_pnt_labuh_selesai_labuh>" + item.selesai.ToString("dd/MM/yyyy HH:mm") + "</upkt_pnt_labuh_selesai_labuh> ");
                sb.Append(" <jenis_kapal>"+item.jenis_kapal +"</jenis_kapal> ");
                sb.Append(" <safm_pelanggan_agen>"+ item.agen+"</safm_pelanggan_agen> ");
                sb.Append("</XMLGen_Renc_Labuh_row>");
            }
            sb.Append("</XMLGen_Renc_Labuh>");
            return sb.ToString();
        }

        public string GenerateXMLPenetapanTambat()
        {
            logger.Info("Start GenerateXMLPenetapanTambat");
            StringBuilder sb = new StringBuilder();
            XMLData xd = new XMLData(_connString);
            IEnumerable<Entity.XMLPenetapanTambat> listTambat = xd.GetPenetapanTambat();

            sb.Append("<?xml version=\"1.0\" ?>");
            sb.Append("<XMLGen_Renc_Tambat>");
            foreach (var item in listTambat)
            {
                sb.Append("<XMLGen_Renc_Tambat_row>");
                sb.Append(" <nama_kapal>"+item.nama_kapal+"</nama_kapal> ");
                sb.Append(" <bendera>"+item.bendera +"</bendera> ");
                sb.Append(" <mulai_tambat>" + item.mulai.ToString("dd/MM/yyyy HH:mm") + "</mulai_tambat> ");
                sb.Append(" <selesai_tambat>" + item.selesai.ToString("dd/MM/yyyy HH:mm") + "</selesai_tambat> ");
                sb.Append(" <jenis>" + item.jenis_kapal + "</jenis> ");
                sb.Append(" <agen>" + item.agen + "</agen> ");
                sb.Append(" <dermaga>" + item.dermaga + "</dermaga>");
                sb.Append("</XMLGen_Renc_Tambat_row>");
            }
            sb.Append("</XMLGen_Renc_Tambat>");
            return sb.ToString();
        }

        public void WriteToFile(string xmlResult, string xmlName) {            
            logger.Info("Write XML to " + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\" + xmlName + ".xml");
            File.WriteAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\" + xmlName + ".xml", xmlResult);
        }

        public void UploadFile(string fileNameToUpload) {
            try
            {
                logger.Info("Start upload " + fileNameToUpload);
                string ftpHost = ConfigurationManager.AppSettings["ftpHost"].ToString();
                string ftpUserName = ConfigurationManager.AppSettings["ftpUserName"].ToString();
                string ftpPassword = ConfigurationManager.AppSettings["ftpPassword"].ToString();
                string ftpXMLDirectory = ConfigurationManager.AppSettings["ftpXMLDirectory"].ToString();

                SessionOptions sessionOpt = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = ftpHost,
                    UserName = ftpUserName,
                    Password = ftpPassword,
                    GiveUpSecurityAndAcceptAnySshHostKey = true
                };

                using (Session session = new Session())
                {
                    session.Open(sessionOpt);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\" + fileNameToUpload, ftpXMLDirectory, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        logger.Info("Upload of " + transfer.FileName + " succeeded");
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }    
    }
}
